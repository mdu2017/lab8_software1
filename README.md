Tasks to answer in your own README.md that you submit on Canvas:

Git repository: https://mdu2017@bitbucket.org/mdu2017/lab8_software1.git


1.  See logger.log, why is it different from the log to console?
    - The log lists out specific info on different levels, but on the console displays all the errors/exceptions
1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
    - It comes from the test code/imported libraries in Tester, the logger writes this the the log.
1.  What does Assertions.assertThrows do?
    - It makes an assertion that the given executable throws an exception.
1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
        - It associates each class with a serializable version ID to determine if classes are compatible/valid during deserialization.
    2.  Why do we need to override constructors?
        - Because it allows for more flexibility in various situations when creating objects.
    3.  Why we did not override other Exception methods?
        - The other exception methods did not need any new functionality.
1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
    - It is the logger object, it is used when writing log messages.
1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
    - The README.md file is a markdown format, and it is used by bitbucket to generate a summary at the bottom.
1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
    - There is a null pointer exception, we need to change the finally to a try/catch block
1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
    - Testing the timers passing, failing, and the edge case. All tests throw TimerExceptions
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel)
    - done
1.  Make a printScreen of your eclipse Maven test run, with console
    - done
1.  What category of Exceptions is TimerException and what is NullPointerException
    - Both exceptions are a type of RunTimeException
1.  Push the updated/fixed source code to your own repository.
    - done
